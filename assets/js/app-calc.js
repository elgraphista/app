// App Calc
const sendInfoForm = document.getElementById('sendInfo');

const montoOrigen = document.getElementById('montoOrigen');
const monedaOrigen = document.getElementById('monedaOrigen');
const montoDestino = document.getElementById('montoDestino');
const monedaDestino = document.getElementById('monedaDestino');

const valorActualMoneda = document.getElementById('rate')

function calculaMontos() {
    const monedaOrigenSel = monedaOrigen.value;
    const monedaDestinoSel = monedaDestino.value;

    fetch(`https://v6.exchangerate-api.com/v6/b5d879d45de38af6ffecca3c/latest/${monedaOrigenSel}`)
    .then(res => res.json())
    .then(data => {
        // console.log(data);
        const rate = data.conversion_rates[monedaDestinoSel];
        valorActualMoneda.innerText = `1 ${monedaOrigenSel} = ${rate} ${monedaDestinoSel}`;
        // montoDestino.value = (montoOrigen.value * rate).toFixed(1);
        montoDestino.value = Math.floor((montoOrigen.value * rate).toFixed(1));
    })
    
}

function intercambiaMontos() {
 console.log('Intercambia Montos')
}

function sendInfo(e) {
    e.preventDefault();

    let montoOrigen = document.getElementById('montoOrigen').value;
    let monedaOrigen = document.getElementById('monedaOrigen').value;
    let plazoSeleccionado = document.querySelector('input[name="plazoSeleccionado"]:checked').value;
    let montoDestino = document.getElementById('montoDestino').value;
    let monedaDestino = document.getElementById('monedaDestino').value;

    console.log(plazoSeleccionado.value)

    // document.getElementById('montoDestino').value = '123456';
    const data = new FormData();
    data.append('montoOrigen', montoOrigen);
    data.append('monedaOrigen', monedaOrigen);
    data.append('montoDestino', montoDestino);
    data.append('monedaDestino', monedaDestino);
    data.append('plazoSeleccionado', plazoSeleccionado);
      fetch('http://206.81.12.62/calc.php', {
        method:'POST',
        body: data
      })
      .then(function(response) {
        return response.json()
        })
        .then(function (json) {
          document.getElementById('montoDestino').value = json.montoDestino;
          console.log(json)
      })
    }

// Listeners

montoOrigen.addEventListener('input', calculaMontos);
monedaOrigen.addEventListener('change', calculaMontos);
montoDestino.addEventListener('input', calculaMontos);
monedaDestino.addEventListener('change', calculaMontos);

sendInfoForm.addEventListener('submit', sendInfo);

calculaMontos();
intercambiaMontos();