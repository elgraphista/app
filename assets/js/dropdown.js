/* Dropdown */
/* const dropdown = document.getElementById("dropMoney");
const dropdownList = document.getElementById("dropList");
const dropLegend = document.getElementById("dropLegend");


// Listener

dropdown.addEventListener('click', () => {
  dropdownList.classList.toggle('show');
}) */


  // hace click fuera
/*   window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  } */


const dropdown = document.querySelector(".dropdown");
const dropdownLegend = document.querySelector(".dropdown__legend");
const dropdownList = document.querySelector(".dropdown__list");

dropdown.addEventListener("click", () => {
  dropdownList.classList.toggle("show");
})

window.addEventListener("click", (event) => {
  if (!event.target.matches('.dropdown__header')) {
    if (dropdownList.classList.contains('show')) {
        dropdownList.classList.remove('show');
      }
    
    if (event.target.matches('.dropdown__item')) {
      dropdownLegend.innerText = event.target.innerText
    }
  }
})